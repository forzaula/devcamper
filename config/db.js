const mongoose = require('mongoose')

const connectDb = async () =>{
    const conn= await mongoose.connect(
        'mongodb+srv://admin:admin@cluster0.0iw5s.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'

    );
    console.log(`MongoDB connected: ${conn.connection.host}`.cyan.underline.bold)
}

module.exports=connectDb;