
const bootcamp= require('../models/bootcamp')
const errorResponse= require('../utils/errorResponse')
const asyncHandler= require('../middleware/async')




exports.getBootcamps = asyncHandler(async (req,res,next)=>{
        const camps= await bootcamp.find();
        res.status(200).json({success:true,count: camps.length,data:camps})
})


exports.getBootcamp = asyncHandler(async (req,res,next)=>{
        const camp=await bootcamp.findById(req.params.id)
        if(!camp){
             return next(new errorResponse(`Bootcamp not found with id of ${req.params.id}`,404))
        }
        res.status(200).json({success:true,data:camp})
})

exports.postBootcamps = asyncHandler(async (req,res,next)=>{
        const camp= await bootcamp.create(req.body);
        res.status(201).json({
            success:true,
            data:camp
        })
})

exports.updateBootcamps = asyncHandler(async (req,res,next)=>{
        const camp= await bootcamp.findByIdAndUpdate(req.params.id,req.body,{
            new:true,
            runValidators:true
        })
        if(!camp){
            return next(new errorResponse(`Bootcamp not found with id of ${req.params.id}`,404))
        }
        res.status(200).json({success:true,data:camp})
})


exports.deleteBootcamp = asyncHandler(async (req,res,next)=>{
        const camp=await bootcamp.findByIdAndDelete(req.params.id)
        if(!camp){
            return next(new errorResponse(`Bootcamp not found with id of ${req.params.id}`,404))
        }
        res.status(200).json({success:true,data:{message:'removed'}})
})