
const mongoose= require('mongoose');
const slugify= require('slugify');



const BootcampSchema = new mongoose.Schema({
    name: {
        type:String,
        required: [true,'Please add a name'],
        unique: true,
        trim:true,
        maxlength:[50,'name can not be more than 50']
    },
    slug:String,
    description:{
        type:String,
        required:[true,'Please add a description'],
        maxlength:[500,'description can not be more than 500']
    },
    website:String,
    phone: {
        type:String,
        maxlength:[20,'can not be more than 20']
    },
    email:{
        type:String,
        required:[true,'Please add email']
    },
    address:{
        type:String,
        required:[true,'Please add address']
    },
    location:{
        type:{
            type:String,
            enum:['Point'],
            // required:true
        },
        coordinates:{
            type:[Number],
            // required:true,
            index:'2dSphere'
        },
        formattedAddress: String,
        street:String,
        city:String,
        state:String,
        zipcode:String,
        country:String
    },
    careers:{
        type:[String],
        required:true,
        enum:[
            'Web Development',
            'Mobile Development',
            'UI/UX',
            'Data Science',
            'Business',
            'Other'
        ]
    },
    averageRating:{
        type:Number,
        min:[1,'rating must be at least 1'],
        max:[10,'rating must be maximum 10']
    },
    averageCost:Number,
    photo:{
        type:String,
        default:'no-photo.jpg'
    },
    housing:{
        type:Boolean,
        default:false
    },
    jobAssistance:{
        type:Boolean,
        default:false
    },
    jobGuarantee:{
        type:Boolean,
        default:false
    },
    acceptGi:{
        type:Boolean,
        default:false
    },
    createdAt:{
        type:Date,
        default:Date.now
    }
})


// create bootcamp slug from the name
BootcampSchema.pre('save',function (next){
    this.slug= slugify(this.name, {lower:true})
    next();
});


module.exports=mongoose.model('bootcamp',BootcampSchema)