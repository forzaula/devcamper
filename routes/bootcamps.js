const express = require('express')
const {
    getBootcamps,
    getBootcamp,
    postBootcamps,
    updateBootcamps,
    deleteBootcamp
} = require('../controllers/bootcamps')
const router = express.Router()

router
    .route('/')
    .get(getBootcamps)
    .post(postBootcamps)

router
    .route('/:id')
    .get(getBootcamp)
    .delete(deleteBootcamp)
    .put(updateBootcamps)




module.exports = router