const express = require('express')
const dotenv = require('dotenv')
const bootCamps = require('./routes/bootcamps')
const connectDb = require('./config/db')
dotenv.config({path:'./config/config.env'});
const morgan = require ('morgan')
const colors = require ('colors')
const errorHandler= require('./middleware/error')



connectDb()
const app= express()
app.use(express.json())
app.use(morgan('dev'))
app.use('/api/v1/bootcamps',bootCamps)
app.use(errorHandler)










const PORT= 5000
const server=app.listen(PORT,()=> console.log(`server is running on port ${PORT}`.yellow.bold))


process.on('unhandledRejection',(err,promise)=>{
    console.log(`error: ${err.message}`.red)
    server.close(()=>process.exit(1))
})



